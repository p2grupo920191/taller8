#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear_Rhaegal(void *ref, size_t tamano){
    Rhaegal *buf = (Rhaegal *)ref;
    buf->i = 5;
    int j = 0;
    for(j = 0; j< 100; j++){
    	buf->arr[j] = j;
    }
    buf->msg = (char *)malloc(sizeof(char)*1000);
}

void destruir_Rhaegal(void *ref, size_t tamano){
    Rhaegal *buf = (Rhaegal *)ref;
    buf->i = 0;
    for(int j = 0; j< 100; j++){
    	buf->arr[j] = 0.0f;
    }
    free(buf->msg);
}

