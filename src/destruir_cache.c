#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"
#include "slaballoc.h"


//llamar funcion destructor de cada objeto y luego liberar toda memoria asignada
void destruir_cache(SlabAlloc *cache){
 // Slab *arr_sl=cache->slab_ptr;
  int i;
  for (i=0;i< cache->tamano_cache;i++){
     void *obj=(cache->mem_ptr)+(i*cache->tamano_objeto);
     cache->destructor(obj,cache->tamano_objeto);
  }
  for (i=0; i< cache->tamano_cache; i++){
	Slab *ptr = (cache->slab_ptr)+(i*sizeof(Slab));
	(ptr->ptr_data )= NULL;
	(ptr->ptr_data_old) = NULL;
	(ptr->status )= 0;
  }
 cache->nombre=NULL;
 cache->tamano_cache=0;
 cache->cantidad_en_uso=0;
 cache->tamano_objeto=0;
 cache->constructor=NULL;
 cache->destructor=NULL;
  free(cache->slab_ptr);
  free(cache->mem_ptr);
 cache->mem_ptr=NULL;
 cache->slab_ptr=NULL;
  free(cache);

}
