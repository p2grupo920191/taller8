#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"
#include "slaballoc.h"


//busca el slab asociado al objeto y lo marca como DISPONIBLE
void devolver_cache(SlabAlloc *alloc, void *obj){
   Slab *arr_sl=alloc->slab_ptr;
   for(int i=0;i<alloc->tamano_cache;i++){
      if((alloc -> mem_ptr)+(i*alloc->tamano_objeto)==obj){
         arr_sl->status=DISPONIBLE;
      }
   }

}


