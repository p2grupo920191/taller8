#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear_Ejemplo(void *ref, size_t tamano){
    Ejemplo *buf = (Ejemplo *)ref;
    buf->a = 5;
    int i = 0;
    for(i = 0; i< 100; i++){
    	buf->b[i] = i;
    }
    buf->msg = (char *)malloc(sizeof(char)*1000);
}

void destruir_Ejemplo(void *ref, size_t tamano){
    Ejemplo *buf = (Ejemplo *)ref;
    buf->a = 0;
    for(int i = 0; i< 100; i++){
    	buf->b[i] = 0.0f;
    }
    //buf->b = 0.0f;
    free(buf->msg);
}

