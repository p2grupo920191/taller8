OBJS=obj/prueba.o obj/Ejemplo.o obj/drogon.o obj/viserion.o obj/rhaegal.o obj/crear_cache.o obj/destruir_cache.o obj/devolver_cache.o obj/obtener_cache.o obj/stats_cache.o
all: bin/estatico bin/dinamico
#----------------------------------------------------------------------------------------------------
#ESTATICO

bin/estatico: $(OBJS) lib/libslaballoc.a
	gcc -static $(OBJS) lib/libslaballoc.a -o bin/estatico

lib/libslaballoc.a: $(OBJS)
	ar -r lib/libslaballoc.a $(OBJS)

#crear todos los archivos .o
obj/%.o: src/%.c
	gcc -Wall -g -static -c -I include/ $^ -o $@

#obj/prueba.o: src/prueba.c
#	gcc -Wall -c -g -static -I include/ src/prueba.c -o obj/prueba.o
#obj/Ejemplo.o: src/Ejemplo.c
#	gcc -Wall -c -g -static -I include/ src/Ejemplo.c -o obj/Ejemplo.o
#obj/drogon.o: src/drogon.c
#	gcc -Wall -c -g -static -I include/ src/drogon.c -o obj/drogon.o
#obj/viserion.o: src/viserion.c
#	gcc -Wall -c -g -static -I include/ src/viserion.c -o obj/viserion.o
#obj/rhaegal.o: src/rhaegal.c
#	gcc -Wall -c -g -static -I include/ src/rhaegal.c -o obj/rhaegal.o
#obj/crear_cache.o:src/crear_cache.c
#	gcc -Wall -c -g -static -I include/ src/crear_cache.c -o obj/crear_cache.o
#obj/destruir_cache.o:src/destruir_cache.c
#	gcc -Wall -c -g -static -I include/ src/destruir_cache.c -o obj/destruir_cache.o
#obj/devolver_cache.o:src/devolver_cache.c
#	gcc -Wall -c -g -static -I include/ src/devolver_cache.c -o obj/devolver_cache.o
#obj/obtener_cache.o:src/obtener_cache.c
#	gcc -Wall -c -g -static -I include/ src/obtener_cache.c -o obj/obtener_cache.o
#obj/stats_cache.o:src/stats_cache.c
#	gcc -Wall -c -g -static -I include/ src/stats_cache.c -o obj/stats_cache.o

#--------------------------------------------------------------------------------------------------
#DINAMICO

bin/dinamico: $(OBJS) lib/libslaballoc.so
	gcc -o bin/dinamico -I include/ $(OBJS) -L lib/ -Bdynamic -lslaballoc

lib/libslaballoc.so: $(OBJS)
	ld -o  lib/libslaballoc.so $(OBJS) -shared


#crear todos los archivos .o
obj/%.o: src/%.c
	gcc -Wall -g -fPIC -c -I include/ $^ -o $@


#obj/prueba.o: src/prueba.c
#	gcc -Wall -c -g  -fPIC -I include/ src/prueba.c -o obj/prueba.o
#obj/Ejemplo.o: src/Ejemplo.c
#	gcc -Wall -c -g -fPIC -I include/ src/Ejemplo.c -o obj/Ejemplo.o
#obj/drogon.o: src/drogon.c
#	gcc -Wall -c -g -fPIC -I include/ src/drogon.c -o obj/drogon.o
#obj/viserion.o: src/viserion.c
#	gcc -Wall -c -g -fPIC -I include/ src/viserion.c -o obj/viserion.o
#obj/rhaegal.o: src/rhaegal.c
#	gcc -Wall -c -g -fPIC -I include/ src/rhaegal.c -o obj/rhaegal.o
#obj/crear_cache.o:src/crear_cache.c
#	gcc -Wall -c -g -fPIC -I include/  src/crear_cache.c -o obj/crear_cache.o
#obj/destruir_cache.o:src/destruir_cache.c
#	gcc -Wall -c -g -fPIC -I include/ src/destruir_cache.c -o obj/destruir_cache.o
#obj/devolver_cache.o:src/devolver_cache.c
#	gcc -Wall -c -g -fPIC -I include/  src/devolver_cache.c -o obj/devolver_cache.o
#obj/obtener_cache.o:src/obtener_cache.c
#	gcc -Wall -c -g -fPIC -I include/ src/obtener_cache.c -o obj/obtener_cache.o
#obj/stats_cache.o:src/stats_cache.c
#	gcc -Wall -c -g -fPIC -I include/ src/stats_cache.c -o obj/stats_cache.o



run:
	bin/dinamico




.PHONY: clean
clean:
	rm bin/* obj/*

